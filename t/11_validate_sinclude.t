# SPDX-License-Identifier: 0BSD
require "TEST.pl";
TEST::init();

print "1..1\n";

$included2 = TEST::tmpfile(<<"EOF");
<<::system("id")::>>
EOF

$included = TEST::tmpfile(<<"EOF");
<:="A":>
#include $included2
<:="B":>
EOF

$basefile = TEST::tmpfile(<<"EOF");
a
#sinclude $included
b
EOF

$result = `../eperl -P $basefile`;

$want = <<"EOF";
a
="A"
system("id")
="B"
b
EOF
print ($want eq $result ? "ok" : "not ok");

TEST::cleanup();
