# SPDX-License-Identifier: Artistic-1.0-Perl OR GPL-2.0-only
require "TEST.pl";
&TEST::init;

print "1..6\n";

#   setup test files
$testfile1 = &TEST::tmpfile_with_name("page.html", <<"EOT");
some stuff
some more stuff
EOT

$testfile1b = &TEST::tmpfile(<<"EOT");
Content-Type: text/html\r
Content-Length: 27\r
\r
some stuff
some more stuff
EOT

$testfile2 = &TEST::tmpfile_with_name("page2.html", <<"EOT");
some stuff
<? print "foo bar"; !>
some more stuff
EOT

$testfile3 = &TEST::tmpfile(<<"EOT");
Content-Type: text/html\r
Content-Length: 35\r
\r
some stuff
foo bar
some more stuff
EOT

#   test for working forced CGI mode
$tempfile1 = &TEST::tmpfile;
$rc = &TEST::system("../eperl -m c $testfile1 >$tempfile1");
print ($rc == 0 ? "ok\n" : "not ok\n");
$rc = &TEST::system("diff -u $testfile1b $tempfile1");
print ($rc == 0 ? "ok\n" : "not ok\n");

#   test for working implicit CGI mode
$tempfile2 = &TEST::tmpfile;
$rc = &TEST::system("PATH_TRANSLATED=$testfile1 GATEWAY_INTERFACE=CGI/1.1 ../eperl >$tempfile2");
print ($rc == 0 ? "ok\n" : "not ok\n");
$rc = &TEST::system("diff -u $testfile1b $tempfile2");
print ($rc == 0 ? "ok\n" : "not ok\n");

#   test if both are equal
$rc = &TEST::system("diff -u $tempfile1 $tempfile2");
print ($rc == 0 ? "ok\n" : "not ok\n");

#   test if filter mode actually works for embedded Perl 5 blocks
$tempfile3 = &TEST::tmpfile;
&TEST::system("../eperl -m c $testfile2 >$tempfile3");
$rc = &TEST::system("diff -u $tempfile3 $testfile3");
print ($rc == 0 ? "ok\n" : "not ok\n");

&TEST::cleanup;
