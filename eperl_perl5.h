/* SPDX-License-Identifier: Artistic-1.0-Perl OR GPL-2.0-only
**  Copyright (c) 1996,1997,1998 Ralf S. Engelschall <rse@engelschall.com>
*/
#ifndef EPERL_PERL5_H
#define EPERL_PERL5_H 1

/*  first include the standard Perl
    includes designed for embedding   */
#define PERL_NO_GET_CONTEXT     /* for efficiency reasons, see perlguts(3) */
#include <EXTERN.h>
#include <perl.h>

#ifndef aTHX
#define aTHX
#define aTHX_
#define pTHX void
#define pTHX_
#endif

#endif /* EPERL_PERL5_H */
