/* SPDX-License-Identifier: Artistic-1.0-Perl OR GPL-2.0-only
**  Copyright (c) 1996,1997,1998 Ralf S. Engelschall <rse@engelschall.com>
*/
#ifndef EPERL_SECURITY_H
#define EPERL_SECURITY_H 1


/*
 * General security for CGI modes
 */
#define CGI_NEEDS_ALLOWED_FILE_EXT       true
static const char *const allowed_file_ext[] = { ".html", ".phtml", ".eperl", ".ephtml", ".epl", ".pl", ".cgi" };

/*
 * Perl security
 * (BE CAREFUL HERE, THIS CAN MAKE YOUR LIFE HARD!)
 */
#define CGI_MODES_FORCE_TAINTING         false
#define CGI_MODES_FORCE_WARNINGS         false

/*
 * SetUID security checks for CGI modes:
 * You can enable/disable any checked steps here.
 */
#define SETUID_NEEDS_VALID_CALLER_UID    true
#define SETUID_NEEDS_ALLOWED_CALLER_UID  true
#define SETUID_NEEDS_VALID_OWNER_UID     true
#define SETUID_NEEDS_VALID_OWNER_GID     true
#define SETUID_NEEDS_BELOW_OWNER_HOME    true
static const char *const allowed_caller_uid[] = { ALLOWED_CALLER_UID };

/*
 * Action when a SetUID security check failed.
 *
 * Define "DO_FOR_FAILED_STEP" to one of the following:
 *
 * MARK_AND_GO_ON: step is marked as failed and processing goes on.
 *                 BUT: No UID/GID switching takes place!
 *                 (default)
 *
 * STOP_AND_ERROR: immediately stop processing print an error.
 *                 (for the paranoid webmaster who really
 *                  wants to enable ePerl only succeded UID/GID
 *                  switching)
 */
#define MARK_AND_GO_ON      1
#define STOP_AND_ERROR      2
#define DO_FOR_FAILED_STEP  MARK_AND_GO_ON


#endif /* EPERL_SECURITY_H */
