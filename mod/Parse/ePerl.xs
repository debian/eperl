/* SPDX-License-Identifier: Artistic-1.0-Perl OR GPL-2.0-only
**  Copyright (c) 1996,1997 Ralf S. Engelschall, All rights reserved.
*/

#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "eperl.h"


MODULE = Parse::ePerl  PACKAGE = Parse::ePerl


##
##  PP -- Run the preprocessor
##  $buffer = Parse::ePerl::PP($buffer, \@INC);
##
void
PP(cpIn, avINC, cpBegin = "<:", cpEnd = ":>")
    char *cpIn;
    AV *avINC;
    char *cpBegin;
    char *cpEnd;
PROTOTYPE: $@;$$
PPCODE:
{
    /* convert Perl array to C array of pointers to chars */
    size_t n = av_len(avINC) + 1;  // perl arrays have n-1 length ([1, 2, 3] is length 2, [] is length -1)
    char **cppINC = n ? malloc(sizeof(char *) * n) : NULL;
    for (size_t i = 0; i < n; ++i) {
        SV *sv = av_shift(avINC);
        STRLEN l;
        char *cp = SvPV(sv, l);
        cppINC[i] = malloc(l + 1);
        memcpy(cppINC[i], cp, l);
        cppINC[i][l] = '\0';
    }

    /* call the preprocessor */
    char *cpOut = ePerl_PP(cpIn, (const char **)cppINC, n, cpBegin, cpEnd, true);

    /* free cppINC */
    for (size_t i = 0; i != n; ++i)
        free(cppINC[i]);
    free(cppINC);

    if (cpOut != NULL) {
        EXTEND(sp, 1);
        PUSHs(sv_2mortal(newSVpv(cpOut, 0)));
        free(cpOut);
    }
}


##
##  Bristled2Plain -- Convert a sprinkled script to a plain script
##  $buffer = Parse::ePerl::Bristled2Plain($buffer, [$beginDel, $endDel, $caseSens, $convertEnts]);
##
void
Bristled2Plain(cpIn, cpBegin = "<:", cpEnd = ":>", fCase = true, fConvertEntities = false)
    char *cpIn;
    char *cpBegin;
    char *cpEnd;
    bool  fCase;
    bool  fConvertEntities;
PROTOTYPE: $;$$$$
PPCODE:
{
    char *cpOut = ePerl_Sprinkled2Plain(cpIn, cpBegin, cpEnd, fCase, fConvertEntities);
    if (cpOut != NULL) {
        EXTEND(sp, 1);
        PUSHs(sv_2mortal(newSVpv(cpOut, 0)));
        free(cpOut);
    }
}
