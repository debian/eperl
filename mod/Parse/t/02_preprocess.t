# SPDX-License-Identifier: Artistic-1.0-Perl OR GPL-2.0-only

BEGIN { $| = 1; print "1..1\n"; }

use Parse::ePerl;
use Cwd 'abs_path';

open(TMP, ">", "tmpfile");
print TMP <<'EOT';
bar
EOT
close(TMP);

$abs = abs_path("tmpfile");
$in = <<"EOT";
foo
#include tmpfile
#include ../tmpfile
#include $abs
quux
EOT

$test = <<'EOT';
foo
bar
bar
bar
quux
EOT

chdir("t");
$rc = Parse::ePerl::Preprocess({
    Script  => $in,
	Result  => \$out,
	INC     => [ 'One', '..', 'Three' ]
});

if ($rc == 1 and $out eq $test) {
	print "ok 1\n";
}
else {
	print "not ok 1\n";
}

unlink("../tmpfile");
