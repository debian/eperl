# SPDX-License-Identifier: Artistic-1.0-Perl OR GPL-2.0-only
##  Copyright (c) 1997 Ralf S. Engelschall, All Rights Reserved.
#   code stolen from Perl 5.004_04's ExtUtils::Embed because
#   this module is only available in newer Perl versions.

use Config;

sub static_ext {
    unless (scalar @Extensions) {
        @Extensions = sort split /\s+/, $Config{static_ext};
        unshift @Extensions, qw(DynaLoader);
    }
    return @Extensions;
}
sub xsi_body {
    my(@exts) = @_;
    my($pname,@retval,%seen);
    my($dl) = canon('DynaLoader');
    foreach $_ (@exts){
        my($pname) = canon($_);
        my($mname, $cname, $ccode);
        ($mname = $pname) =~ s!/!::!g;
        ($cname = $pname) =~ s!/!__!g;
        if ($pname eq $dl){
            $ccode = "newXS(\"${mname}::boot_${cname}\", boot_${cname}, __FILE__); ";
        } else {
            $ccode = "newXS(\"${mname}::bootstrap\", boot_${cname}, __FILE__); ";
        }
        push(@retval, $ccode) unless $seen{$ccode}++;
    }
    return join '', @retval;
}
sub canon {
    my(@ext) = @_;
    foreach(@ext) {
       # might be X::Y or lib/auto/X/Y/Y.a
       next if s!::!/!g;
       s:^(lib|ext)/(auto/)?::;
       s:/\w+\.\w+$::;
    }
    return @ext;
}
@mods = ();
push(@mods, static_ext());
@mods = grep(!$seen{$_}++, @mods);
print xsi_body(@mods);
