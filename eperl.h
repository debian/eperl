/* SPDX-License-Identifier: Artistic-1.0-Perl OR GPL-2.0-only
**  Copyright (c) 1996,1997,1998 Ralf S. Engelschall <rse@engelschall.com>
*/
#ifndef EPERL_PROTO_H
#define EPERL_PROTO_H 1

#include "config.h"

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef HAVE_SYS_PARAM_H
#include <sys/param.h>
#endif


#ifndef O_PATH
#define O_PATH O_RDONLY
#endif

#define EX_OK   0
#define EX_FAIL 1
#define EX_USAGE        64      /* command line usage error */
#define EX_IOERR        74      /* input/output error */

#define CU(returncode) do { rc = returncode; goto CUS; } while(0)

/* eperl_main.c */
enum runtime_mode {
    MODE_UNKNOWN = 1,
    MODE_FILTER  = 2,
    MODE_CGI     = 4,
    MODE_NPHCGI  = 8,
};
extern void PrintError(enum runtime_mode mode, const char *scripturl, const char *scriptfile, char **errstr, size_t *errstrN, const char *str, ...) __attribute__((format(printf, 6, 7)));

/* eperl_parse.c */
extern bool ePerl_line_continuation;
extern char *ePerl_ErrorString;
extern void *memcasemem(const void *buf, size_t n, const void *str, size_t len);
extern void ePerl_SetError(const char *str, ...) __attribute__((format(printf, 1, 2)));
extern char *ePerl_Sprinkled2Plain(const char *cpBuf, const char *ePerl_begin_delimiter, const char *ePerl_end_delimiter, bool ePerl_case_sensitive_delimiters, bool ePerl_convert_entities);

/* eperl_pp.c */
extern char *ePerl_PP(char *cpBuf, const char *const *cppINC, size_t cppINCLen, const char *ePerl_begin_delimiter, const char *ePerl_end_delimiter, bool ePerl_case_sensitive_delimiters);

/* eperl_sys.c */
extern void putenvf(const char *fmt, ...) __attribute__((format(printf, 1, 2)));
extern void IO_redirect_stdout(int fd);
extern void IO_redirect_stderr(int fd);
extern void IO_restore_stdout(void);
extern void IO_restore_stderr(void);
enum tmpfile_id { tmpfile_stdin, tmpfile_script, tmpfile_stdout, tmpfile_cnt };
struct tmpfile { char *filename; int fd; };
extern struct tmpfile mytmpfile(enum tmpfile_id id);
extern void remove_mytmpfiles(void);
extern bool ePerl_CopyFILE(FILE *from, FILE *to);
extern bool  ePerl_ReadSourceFile(const char *filename, char **cpBufC, size_t *nBufC);
extern void ePerl_SubstErrorLog(char **cpBuf, size_t *nBuf, const char *replace, const char *with);

/* eperl_http.c */
extern size_t HTTP_PrintResponseHeaders(const char *cpBuf);
extern bool HTTP_HeadersExists(const char *cpBuf);
extern FILE *HTTP_openURLasFP(const char *url);

/* eperl_perl5.c */
extern void Perl5_RememberScalar(char *str);
extern int Perl5_Run(int myargc, char **myargv, enum runtime_mode mode, bool fCheck, bool keepcwd, int *cwd,
	                 const char *sourcedir, const char *source, const char *perlscript, char **stdoutBuf, size_t *nstdoutBuf);


extern const size_t  ePerl_README_size;
extern const size_t  ePerl_LICENSE_size;
extern const size_t  ePerl_LOGO_size;
extern const size_t  ePerl_POWERED_size;
extern const uint8_t ePerl_README[];
extern const uint8_t ePerl_LICENSE[];
extern const uint8_t ePerl_LOGO[];
extern const uint8_t ePerl_POWERED[];

#endif /* EPERL_PROTO_H */
